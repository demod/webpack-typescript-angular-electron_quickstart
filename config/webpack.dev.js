var path = require('path');
var fs = require('fs');

var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var helpers = require('./helpers');

console.log(path.resolve(__dirname, '../build'));

module.exports = {
    entry: {
        polyfills: './source/polyfills',
        vendor: './source/vendor',
        app: './source/app'
    },
    output: {
        path: path.resolve(__dirname, '../build'),
        filename: '[name].js',
    },

    resolve: {
        extensions: ['.js', '.ts'],
        modules: [
            'node_modules'
        ]
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                loader:  [
                    {
                        loader: 'awesome-typescript-loader',
                        options: {
                            configFileName: helpers.root('', 'tsconfig.json')
                        }
                    },
                    'angular2-template-loader'
                ]
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            },
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                loader: 'file-loader?name=assets/[name].[hash].[ext]'
            },
            {
                test: /\.css$/,
                exclude: helpers.root('source', 'app'),
                loader: ExtractTextPlugin.extract(
                    {
                        fallback: 'style-loader',
                        use: 'css-loader?sourceMap'
                    }
                )
            },
            {
                test: /\.css$/,
                include: helpers.root('source', 'app'), loader: 'raw-loader'
            }
        ]
    },
    plugins: [
        new webpack.ContextReplacementPlugin(
            /angular(\\|\/)core(\\|\/)(esm(\\|\/)source|source)(\\|\/)linker/,
            helpers.root('./source'), 
            {} 
        ),
        new webpack.optimize.CommonsChunkPlugin(
            {
                name: ['app', 'vendor', 'polyfills']
            }
        ),
        new HtmlWebpackPlugin(
            {
                template: './source/app.html',
                inject: 'body',
            }
        )
    ],

    node: {
        fs: "empty"
    },
    
    devtool: 'source-map'
};


