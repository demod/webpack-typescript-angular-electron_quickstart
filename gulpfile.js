var gulp = require('gulp');
var webpackStream = require("webpack-stream");
var webpack = require("webpack");
var webpackConfig = require('./webpack.config.js');


/**
 * =============   TASKS   =============
 */



/**
 * Default Task
 */
gulp.task('default', ['watchFiles']);

gulp.task('build-dev', ['build-app-webpack'], function() {});
gulp.task('build-production', ['build-app-webpack'], function() {});

/**
 * File watcher
 */
gulp.task('watchFiles', ['build-dev'], function() {
    gulp.watch('source/**/*', ['build-dev']);
});




gulp.task('build-app-webpack', function(){
    return gulp.src('./source/app.ts')
        .pipe(
            webpackStream (
                webpackConfig,
                webpack
            )
        )
        .pipe(
            gulp.dest('./build/')
        );
});
