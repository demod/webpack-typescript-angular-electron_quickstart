import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, JsonpModule } from '@angular/http';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';

import { AppComponent }   from './app.component';


/**
 * Main app module
 */
@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        JsonpModule
    ],
    exports: [],
    declarations: [
        AppComponent
    ],
    providers: [],
    bootstrap: [
        AppComponent
    ]
})

export class AppModule {}